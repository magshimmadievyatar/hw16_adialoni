#pragma once
#include <stdio.h>
#include "sqlite3.h"
#include <string>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

unordered_map<string, vector<string>> results;
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int main()
{
	int rc;
	sqlite3* db;
	char* errMsg = 0;

	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	rc = sqlite3_exec(db, "drop table people", callback, 0, &errMsg);
	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name string)", callback, 0, &errMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << errMsg << endl;
		sqlite3_free(errMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, "insert into people(name) values(\"Hisoka\")", callback, 0, &errMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << errMsg << endl;
		sqlite3_free(errMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, "insert into people(name) values(\"Illumi\")", callback, 0, &errMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << errMsg << endl;
		sqlite3_free(errMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, "insert into people(name) values(\"Killua\")", callback, 0, &errMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << errMsg << endl;
		sqlite3_free(errMsg);
		system("Pause");
		return 1;
	}


	rc = sqlite3_exec(db, "	update people set name = \"Gon\" where id = 3", callback, 0, &errMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << errMsg << endl;
		sqlite3_free(errMsg);
		system("Pause");
		return 1;
	}
	cout << "All done" << endl;
	return 0;
}