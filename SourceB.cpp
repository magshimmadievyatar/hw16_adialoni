#pragma once
#include <stdio.h>
#include "sqlite3.h"
#include <string>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <sstream>

using namespace std;

unordered_map<string, vector<string>> results;
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	const char* s = 0;
	stringstream ss;
	ss << "select balance from accounts where Buyer_id=" << buyerid;
	string str = ss.str();
	s = str.c_str();
	rc = sqlite3_exec(db, s, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	pair<string, vector<string>> p = *(results.begin());
	string amountOfMoney = p.second[0];
	int realAmount = atoi(amountOfMoney.c_str());
	results.clear();
	ss.str(string());
	ss << "select price from cars where id=" << carid;
	str = ss.str();
	s = str.c_str();
	rc = sqlite3_exec(db, s, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	p = *(results.begin());
	string cost = p.second[0];
	int realCost = atoi(cost.c_str());
	results.clear();
	if (realAmount > realCost)
	{
		ss.str(string());
		ss << "select available from cars where id=" << carid;
		str = ss.str();
		s = str.c_str();
		rc = sqlite3_exec(db, s, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		p = *(results.begin());
		string availability = p.second[0];
		int realAvailability = atoi(availability.c_str());
		if (realAvailability)
			return true;
	}
	return false;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	const char* s = 0;
	stringstream ss;
	ss << "select balance from accounts where Buyer_id=" << from;
	string str = ss.str();
	s = str.c_str();
	rc = sqlite3_exec(db, s, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	pair<string, vector<string>> p = *(results.begin());
	string balanceOne = p.second[0];
	int realBalanceOne = atoi(balanceOne.c_str());
	results.clear();
	ss.str(string());
	ss << "select balance from accounts where Buyer_id=" << to;
	str = ss.str();
	s = str.c_str();
	rc = sqlite3_exec(db, s, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	p = *(results.begin());
	string balanceTwo = p.second[0];
	int realBalanceTwo = atoi(balanceTwo.c_str());
	results.clear();
	ss.str(string());
	if (realBalanceOne - amount > 0)
	{
		ss << "update accounts set balance =" << realBalanceOne - amount << " where id=" << from;
		str = ss.str();
		s = str.c_str();
		rc = sqlite3_exec(db, s, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		ss.str(string());
		ss << "update accounts set balance =" << realBalanceTwo + amount << " where id=" << to;
		str = ss.str();
		s = str.c_str();
		rc = sqlite3_exec(db, s, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		ss.str(string());
		ss << "select balance from accounts where Buyer_id=" << from;
		str = ss.str();
		s = str.c_str();
		rc = sqlite3_exec(db, s, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		p = *(results.begin());
		string newbalanceOne = p.second[0];
		int newrealBalanceOne = atoi(newbalanceOne.c_str());
		results.clear();
		ss.str(string());
		ss << "select balance from accounts where Buyer_id=" << to;
		str = ss.str();
		s = str.c_str();
		rc = sqlite3_exec(db, s, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		p = *(results.begin());
		string newbalanceTwo = p.second[0];
		int newrealBalanceTwo = atoi(newbalanceTwo.c_str());
		results.clear();
		ss.str(string());
		if (newrealBalanceOne + amount == realBalanceOne && newrealBalanceTwo - amount == realBalanceTwo)
			return true;
		else
		{
			cout << "Something went wrong" << endl;
		}
	}
	return false;
}


int main()
{
	int rc = 0;
	sqlite3* db = 0;
	char* errMsg = 0;
	rc = sqlite3_open("C:\\Users\\magshimim\\source\\repos\\HW16_AdiAloni\\HW16_AdiAloni\\carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	cout << "All good sir!" << endl;
	system("pause");
	return 0;
}